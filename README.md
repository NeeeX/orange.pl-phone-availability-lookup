# Orange.pl Phone Availability Lookup #

The application is used to lookup whether desired phone is available in Orange.pl online store.
It's useful in scenario where the mobile device had just been released and is rarely available in the online store.