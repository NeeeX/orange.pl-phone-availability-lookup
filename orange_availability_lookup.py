import requests
from BeautifulSoup import BeautifulSoup
from datetime import datetime


class Phone:

    def __init__(self, name='', is_available=False):
        self.name = name
        self.is_available = is_available


#Sends AJAX request to Orange systems asking to show list of iPhones in their offerings
#Returns the string containing html, only when the server responded with 200 HTTP Code
def send_phones_fetch_request():
    sess = requests.Session()
    sess.headers[
        'User-Agent'] = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                        'Chrome/38.0.2125.111 Safari/537.36'

    post = 'toUpdate=product-list-content&toGet=product-list-content&link=/klienci_ind/telefony&addJsLiveChat=' \
           '&_dyncharset=UTF-8&_dynSessConf=6589119676691559788&device=PHONE&=submit&=submit&=%2Fprt%2Fpl%2Fkli' \
           'enci_ind%2Fsklep%2Ftelefony_stacjonarne%2Ftel_bezprz&=submit&=%2Fprt%2Fpl%2Fklienci_ind%2Fsklep%2F' \
           'modemy_neostrada_orange%2Fwszystkie&=http%3A%2F%2Fakcesoria.orange.pl%2Fsklep%2Ckategoria%2Cid%3D-' \
           '1%2Cod%3D50%2Cdo%3D200&=submit&=%2Fprt%2Fpl%2Fklienci_ind%2Fsklep%2Fgadzet%2Fbrandshop&processType=new&' \
           'brand=POSTPAID&proposition=8503564&commitment=94.9929&loyalty=24&producer=&producer=&producer=&' \
           'producer=&producer=&producer=&producer=31&producer=&os=&os=&os=&function=&function=&function=&function=&' \
           'function=&function=&function=&communicationStandard=&promotion=&promotion=&promotion=&promotion=&' \
           'usertype=&terminal_price_range=1%2C1%2C2679%2C2679&=submit&market=B2C&cute_link_url=%2Fklienci_ind' \
           '%2Ftelefony%3Fproducer%3D31&paf_page_name=Phones' \
           '&_DARGS=/gear/commerce/shop/product_list/ajaxify/product_list_content.jsp'

    sess.headers['X-Requested-With'] = 'XMLHttpRequest'
    sess.headers['X-Prototype-Version'] = '1.7'
    sess.headers['Origin'] = 'http://www.orange.pl'
    sess.headers['Content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    sess.headers['Accept'] = 'text/javascript, text/html, application/xml, text/xml, */*'
    sess.headers['Referer'] = 'http://www.orange.pl/klienci_ind/telefony?producer=31'
    sess.headers['Accept-Language'] = 'pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4'

    r = sess.post('http://www.orange.pl/gear/commerce/ajax', post)

    return r.content if r.status_code == 200 else None


#Fetches the list of phones offered by Orange in form of Phone() objects.
#Returns the list of Phone() objects or False if unable to fetch or extract the data from html
def get_phones_pricing():
    phones_html = send_phones_fetch_request()

    return extract_phones_from_html(phones_html) if phones_html else None


#Parses the html trying to extract the phones from html input text. For each phone - Phone() object is created
#and filled with relevant data. Returns the list of Phone() objects
def extract_phones_from_html(html):
    soup = BeautifulSoup(html)
    divs = soup.findAll("div", "product-content")

    found_phones = []

    for d in divs:
        phone = Phone()
        phone.name = ((d.find("div").get("id")).split('-'))[2]
        phone.is_available = False if d.find("span", "unavailable-text") else True
        found_phones.append(phone)

    return found_phones


#Append information about desired phone being found to a text file
def log_found_event_to_file(path, model):
    with open(path, 'a+') as f:
        f.writelines("[%s] - Model %s is available\n"
                     % (datetime.now().isoformat(),
                        model)
                     )

target_phones = ['IPHONE6PLUS_64GB', 'IPHONE6_64GB']
orange_phones = get_phones_pricing()

if orange_phones:
    for phone in orange_phones:
        if phone.name in target_phones and phone.is_available is True:
            log_found_event_to_file('orange_phone_availibility.txt', phone.name)
